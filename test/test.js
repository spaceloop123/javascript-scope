const assert = require('assert');
const tasks = require('../src');

describe('09-functions-n-closures-tasks', function() {

    it('retry method should try to evaluate the specified function several times', () => {
        var maxAttemps = 3;
        var attemps = 0;
        var expected = 'expected';

        var fn = function() {
            if (++attemps<maxAttemps) throw new Error();
            return expected;
        }

        var actual = tasks.retry(fn, maxAttemps)();
        assert.equal(actual, expected);
    });


    it('logger method should log start and end of call of the standard js function', () => {
        var log = '';

        var logFunc = (text) => ( log += text + '\n');
        var cosLogger = tasks.logger(Math.cos, logFunc);

        var actual = cosLogger(Math.PI);

        assert.equal(actual, -1, 'logger function should return the original result from specified function');
        assert.equal(
            log,
            'cos(3.141592653589793) starts\n'
            +'cos(3.141592653589793) ends\n',
            'logger function shoud log the start and end of the specified function');
    });


    it('logger method should log start and end of call of the specified function', () => {
        var isCalling = false;
        var log = '';

        var fn = function testLogger(param, index) {
            assert.equal(
                log,
                'testLogger(["expected","test",1],0) starts\n',
                'logger function shoud log the start of specified function before calling'
            );
            isCalling = true;
            return param[index];
        }

        var logFunc = (text) => ( log += text + '\n');
        var logger = tasks.logger(fn, logFunc);

        var actual = logger(["expected", "test", 1], 0);

        assert.equal(isCalling, true, 'logger function should call the specified function');
        assert.equal(actual, 'expected', 'logger function should return the original result from specified function');
        assert.equal(
            log,
            'testLogger(["expected","test",1],0) starts\n'
            +'testLogger(["expected","test",1],0) ends\n',
            'logger function shoud log the end of specified function after calling');
    });


    it('partialUsingArguments should return the function with partial applied arguments', () => {
        const fn = (x1,x2,x3,x4) => x1+x2+x3+x4;
        assert.equal(
            tasks.partialUsingArguments(fn, 'a')('b','c','d'),
            'abcd',
            "partialUsingArguments(fn, 'a')('b','c','d')' should return 'abcd'"
        );
        assert.equal(
            tasks.partialUsingArguments(fn, 'a','b')('c','d'),
            'abcd',
            "partialUsingArguments(fn, 'a','b')('c','d')' should return 'abcd'"
        );
        assert.equal(
            tasks.partialUsingArguments(fn, 'a','b','c')('d'),
            'abcd',
            "partialUsingArguments(fn, 'a','b','c')('d') should return 'abcd'"
        );
        assert.equal(
            tasks.partialUsingArguments(fn, 'a','b','c','d')(),
            'abcd',
            "partialUsingArguments(fn, 'a','b','c','d')()' should return 'abcd'"
        );
    });


    it('getIdGeneratorFunction should return the id generator function', () => {

        var f0 = tasks.getIdGeneratorFunction(0);
        for(var i=0; i<1000; i++) {
            assert.equal(f0(), i);
        }

        var f10 = tasks.getIdGeneratorFunction(10);
        var f20 = tasks.getIdGeneratorFunction(20);
        for(var i=0; i<1000; i++) {
            assert.equal(f10(), 10+i);
            assert.equal(f20(), 20+i);
        }
    });

});

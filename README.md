# JavaScript Scope

## Tasks

#### Retry
Return the function trying to call the passed function and if it throws, retrying it specified number of attempts.
For example:
```js
var attempt = 0, retryer = retry(() => {
    if (++attempt % 2) throw new Error('test');
    else return attempt;
}, 2);
retryer() => 2
```
Write your code in `src/index.js` within an appropriate function `retry`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Logger
Return the logging wrapper for the specified method,
Logger has to log the start and end of calling the specified function.
Logger has to log the arguments of invoked function.
The fromat of output log is:

<function name>(<arg1>, <arg2>,...,<argN>) starts

<function name>(<arg1>, <arg2>,...,<argN>) ends

For example:
```js
var cosLogger = logger(Math.cos, console.log);
var result = cosLogger(Math.PI));     // -1

log from console.log:
cos(3.141592653589793) starts
cos(3.141592653589793) ends
```
Write your code in `src/index.js` within an appropriate function `logger`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Carrying
Return the function with partial applied arguments.
For example:
```js
var fn = function(x1,x2,x3,x4) { return  x1 + x2 + x3 + x4; };
partialUsingArguments(fn, 'a')('b','c','d') => 'abcd'
partialUsingArguments(fn, 'a','b')('c','d') => 'abcd'
partialUsingArguments(fn, 'a','b','c')('d') => 'abcd'
partialUsingArguments(fn, 'a','b','c','d')() => 'abcd'
```
Write your code in `src/index.js` within an appropriate function `partialUsingArguments`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Id Generator
Return the id generator function that returns next integer starting from specified number every time when invoking.
For example:
```js
var getId4 = getIdGenerator(4);
var getId10 = gerIdGenerator(10);
getId4() => 4
getId10() => 10
getId4() => 5
getId4() => 6
getId4() => 7
getId10() => 11
```
Write your code in `src/index.js` within an appropriate function `getIdGenerator`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

## Prepare and test
1. Install [Node.js](https://nodejs.org/en/download/)   
2. Fork this repository: javascript-scope
3. Clone your newly created repo: https://gitlab.com/<%your_gitlab_username%>/javascript-scope/  
4. Go to folder `javascript-scope`  
5. To install all dependencies use [`npm install`](https://docs.npmjs.com/cli/install)  
6. Run `npm test` in the command line  
7. You will see the number of passing and failing tests you 100% of passing tests is equal to 100p in score  

## Submit to [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/)
1. Open [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/) and login
2. Navigate to the JavaScript Mentoring Course page
3. Select your task (javascript-scope)
4. Press the submit button and enjoy, results will be available in few minutes

### Notes
1. We recommend you to use nodejs of version 12 or lower. If you using are any of the features which are not supported by v12, the score won't be submitted.
2. Each of your test case is limited to 30 sec.
